$(function() {
	'use strict';
	$(window).resize(function() {
		if ($(window).width() > 768) {
			if(!$("#navlist").show()) {
				$("#navlist").show();
			}
		}
	});
	$('ul.tabs li').click(function() {		
		const tab_id = $(this).attr('data-tab');		
		$('ul.tabs li').removeClass('current');
		$('.project-detail-full').removeClass('current');
		$('.projects-by-category').removeClass('current');
		$(this).addClass('current');
		$("#tab-"+tab_id).addClass('current');		
	});
	createSwiper();
	initMobileNavbar();
	matchMyHeight();
	modalCreate();
	countCategoriedProject();
});
function createSwiper() {
	const swiperHeader = new Swiper('.header-slider', {
		pagination: '.swiper-pagination',
		paginationClickable: true,
		nextButton: '.swiper-button-next',
		prevButton: '.swiper-button-prev',
		speed: 700,
		autoplay: 5000
	});
	const partnerSwiper = new Swiper('.partner-slider', {
		slidesPerView: 4,
		spaceBetween: 30,
		autoplay: 4500,
		breakpoints: {			
			768: {
				slidesPerView: 3,
				spaceBetween: 40,
			},
			545: {
				slidesPerView:2,
				spaceBetween: 50
			}
		}
	})
}
function initMobileNavbar() {
	const t = $("#navlist");
	$("#mobile-nav-toggle").click(function() {
		t.is(":visible") || t.hasClass("velocity-animating") ? t.velocity ("slideUp", {
			duration: 300
		}) : t.velocity("slideDown", {
			duration: 300
		})
	})
}
function matchMyHeight() {
	$('.one-button-img').matchHeight({
		byRow:true,
		property: 'height',
		target:null,
		remove:false
	});
}
function modalCreate() {
	const modal = $('#signupmodal');
	const button = $('.modal-open');
	button.on("click", function() {
		modal.show();
		$('#modalbackdrop').show();    
	});
	$('#modalbackdrop').click(function(event) {
		event.preventDefault();
		$('#modalbackdrop, #signupmodal').hide();
	});
}

function countCategoriedProject() {
	const classCount = $('.project-one-by-category').length;
	if ($(window).width() > 768) {
		switch(classCount%3) {
			case 0:
			$('.project-one-by-category:nth-last-child(-n+3)').css({"margin-bottom": 0});

			break
			case 1:
			$('.project-one-by-category:nth-last-child(-n+1)').css({"margin-bottom": 0});
			break;
			case 2:
			$('.project-one-by-category:nth-last-child(-n+2)').css({"margin-bottom": 0});
			break;

		}
	}
}