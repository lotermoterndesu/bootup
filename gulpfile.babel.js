'use strict';

import gulp from 'gulp';
import sass from 'gulp-sass';
import autoprefixer from 'gulp-autoprefixer';
import clean from 'gulp-clean-css';
import minify from 'gulp-minify';
import jsfuck from 'gulp-jsfuck';
const browsify = require('browser-sync').create();


const paths = {
  styles: {
    src: 'stylesheets/scss/main.scss',
    dest: 'stylesheets/'
  },
  scripts: {
    src: 'scripts/main.js',
    dest: 'scripts/app.js'
  }
};
// gulp.task('scripts', () => {
//   return gulp.src(paths.scripts.src)
//         .pipe(minify())
//         .pipe(gulp.dest(paths.scripts.dest))
// });
gulp.task('styles', () => {
  return gulp.src(paths.styles.src)
        .pipe(sass.sync().on('error', sass.logError))
        .pipe(autoprefixer())
        .pipe(gulp.dest(paths.styles.dest))
        // .pipe(clean({compability: 'ie8'}))
        .pipe(gulp.dest(paths.styles.dest))
        .pipe(browsify.stream());
});
gulp.task('watch', () => {
  browsify.init({
    server: {
      baseDir: "."
    }
  });
  gulp.watch(paths.styles.src, ['styles']);  
  gulp.watch(["index.html", "page3.html", "page4.html", "stylesheets/main.css", "scripts/main.js"]).on("change", browsify.reload);  
});
gulp.task('default', ['watch', 'styles']);
